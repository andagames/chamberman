﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class NavigationController : MonoBehaviour {

    public GameObject GoldText;

    public GameObject ElementText1;
    public GameObject ElementText2;
    public GameObject ElementText3;
    public GameObject ElementText4;
    public GameObject ElementText5;
    public GameObject ElementText6;
    public GameObject ElementText7;
    public GameObject ElementText8;

    public GameObject PilotNumberText;
    public GameObject BiologNumberText;
    public GameObject SoldierNumberText;
    public GameObject AssasinNumberText;
    public GameObject DoctorNumberText;


    // Use this for initialization
    void Start () {

        GoldText.GetComponent<TextMeshProUGUI>().text = (PlayerPrefs.GetInt(PlayerPrefConstanst.GOLD_COUNT)).ToString();

        ElementText1.GetComponent<TextMeshProUGUI>().text = (PlayerPrefs.GetInt(PlayerPrefConstanst.Element1)).ToString();
        ElementText2.GetComponent<TextMeshProUGUI>().text = (PlayerPrefs.GetInt(PlayerPrefConstanst.Element2)).ToString();
        ElementText3.GetComponent<TextMeshProUGUI>().text = (PlayerPrefs.GetInt(PlayerPrefConstanst.Element3)).ToString();
        ElementText4.GetComponent<TextMeshProUGUI>().text = (PlayerPrefs.GetInt(PlayerPrefConstanst.Element4)).ToString();
        ElementText5.GetComponent<TextMeshProUGUI>().text = (PlayerPrefs.GetInt(PlayerPrefConstanst.Element5)).ToString();
        ElementText6.GetComponent<TextMeshProUGUI>().text = (PlayerPrefs.GetInt(PlayerPrefConstanst.Element6)).ToString();
        ElementText7.GetComponent<TextMeshProUGUI>().text = (PlayerPrefs.GetInt(PlayerPrefConstanst.Element7)).ToString();
        ElementText8.GetComponent<TextMeshProUGUI>().text = (PlayerPrefs.GetInt(PlayerPrefConstanst.Element8)).ToString();

        PilotNumberText.GetComponent<TextMeshProUGUI>().text = (PlayerPrefs.GetInt(PlayerPrefConstanst.PilotNumber)).ToString();
        BiologNumberText.GetComponent<TextMeshProUGUI>().text = (PlayerPrefs.GetInt(PlayerPrefConstanst.BiologNumber)).ToString();
        SoldierNumberText.GetComponent<TextMeshProUGUI>().text = (PlayerPrefs.GetInt(PlayerPrefConstanst.SoldierNumber)).ToString();
        AssasinNumberText.GetComponent<TextMeshProUGUI>().text = (PlayerPrefs.GetInt(PlayerPrefConstanst.AssasinNumber)).ToString();
        DoctorNumberText.GetComponent<TextMeshProUGUI>().text = (PlayerPrefs.GetInt(PlayerPrefConstanst.DoctorNumber)).ToString();


    }

    // Update is called once per frame
    void Update () {
		
	}
}
