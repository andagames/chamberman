﻿using UnityEngine;

    public class Creature: MonoBehaviour
    {

    public Rigidbody2D myRigidBody;

    public bool isCollidedWithDivineShield = false;
    public bool isCollidedWithFireCircle = false;
    
    public static int speedDivisorForSlowerObjectsJoker = 1;

    public float MoveSpeedX;
    public float MoveSpeedY;
    
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag.Equals(Constants.STR_SHIELD))
        {
            isCollidedWithDivineShield = true;
        }
        if (collider.gameObject.tag.Equals(Constants.STR_FIRE_CIRCLE))
        {
            isCollidedWithFireCircle = true;
        }
    }

    public void checkAffectedSpeed()
    {
        if (SessionParameters.isStopGameClicked || SessionParameters.isGameOver || SessionParameters.isFreezeThemAllClicked)
        {
            myRigidBody.velocity =
                   new Vector2(Numbers.ZERO, Numbers.ZERO);
        }
        else
        {
            if (SessionParameters.isFastGameClicked) {
                myRigidBody.velocity = new Vector2(
                    MoveSpeedX * Constants.FAST_GAME_MODE_CREATURE_SPEED_MULTIPLIER / speedDivisorForSlowerObjectsJoker,
                   MoveSpeedY * Constants.FAST_GAME_MODE_CREATURE_SPEED_MULTIPLIER /  speedDivisorForSlowerObjectsJoker);
            }
            else
            {
                myRigidBody.velocity = new Vector2(MoveSpeedX / speedDivisorForSlowerObjectsJoker, 
                    MoveSpeedY / speedDivisorForSlowerObjectsJoker);
            }
        }
    }

    // eğer enemy ekranın dısındaysa, onu destroy et
    public void checkOutOfScene()
    {
        float xLocation = myRigidBody.position.x;
        float yLocation = myRigidBody.position.y;

        if (!(xLocation >= Constants.X_MIN_FOR_CREATURE) || !(xLocation <= Constants.X_MAX_FOR_CREATURE)
            || !(yLocation >= Constants.Y_MIN_FOR_CREATURE) || !(yLocation <= Constants.Y_MAX_FOR_CREATURE))
        {
            Destroy(this.gameObject);
        }
    }

    // fire circle ile çarpısırsa
    public void checkIsCollidedWithFireCircle()
    {
        if (isCollidedWithFireCircle)
        {
            Destroy(this.gameObject);
        }
    }

    public void checkIsSlowerObjectsClicked()
    {
        if (SessionParameters.isSlowerObjectsClicked)
        {
            speedDivisorForSlowerObjectsJoker = 2;
        }
        else
        {
            speedDivisorForSlowerObjectsJoker = 1;
        }
    }

    // enemylerle çarpıştığı için butun enemyler yok edilir
    public static void destroyAllCreatures()
    {
        Transform[] gameObjs = FindObjectsOfType(typeof(Transform)) as Transform[];
        foreach (Transform transform in gameObjs)
        {
            if (transform.tag.Equals(Constants.STR_CREATURE) || transform.tag.Equals(Constants.STR_ENERGY))
            {
                Destroy(transform.gameObject);
            }
        }
    }
    
    public static void freezeAllCreatures()
    {
        Transform[] gameObjs = FindObjectsOfType(typeof(Transform)) as Transform[];
        foreach (Transform transform in gameObjs)
        {
            if (transform.tag.Equals(Constants.STR_CREATURE))
            {
                transform.GetComponent<Rigidbody2D>().velocity = new Vector2(Numbers.ZERO, Numbers.ZERO);
            }
        }
    }
}
