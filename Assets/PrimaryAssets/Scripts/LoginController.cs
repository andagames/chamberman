﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoginController : MonoBehaviour
{

    public InputField userNameField;
    public InputField passwordField;

    public GameObject LoginScreen;
    public GameObject SettingsScreen;
    // Use this for initialization
    void Start()
    {
        LoginScreen.SetActive(true);
        SettingsScreen.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        if (hit.collider != null)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Debug.Log("current hit: " + hit.collider.name);
                if (hit.collider.name.Equals("LoginButton"))
                {
                    Debug.Log("Login button clicked");
                    string username = userNameField.GetComponent<InputField>().text;
                    string password = passwordField.GetComponent<InputField>().text;
                    Debug.Log("username: " + username);
                    Debug.Log("password: " + password);

                    if (username.Equals("cemal") && password.Equals("cemal"))
                    {
                        Debug.Log("Credentials are true");
                        SceneManager.LoadScene("Navigation2");
                        /*
                        string url = Constants.url + "/user/insertUserInfo/";
                        WWWForm form = new WWWForm();
                        form.AddField("username", username);
                        // Create a download object
                        WWW download = new WWW(url, form);
                        StartCoroutine(InsertUser(download));
                        StopCoroutine("InsertUser");
                        */
                    }

                }
                else if (hit.collider.name.Equals("SettingsButton"))
                {
                    LoginScreen.SetActive(false);
                    SettingsScreen.SetActive(true);
                }
                else if (hit.collider.name.Equals("SettingsCloseButton"))
                {
                    LoginScreen.SetActive(true);
                    SettingsScreen.SetActive(false);
                }
                else if (hit.collider.name.Equals("QuitButton"))
                {
                    Application.Quit();
                }
            }
        }
    }
}