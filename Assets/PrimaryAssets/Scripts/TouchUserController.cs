﻿using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof(TouchControllerCharacter))]
    public class TouchUserController : MonoBehaviour
    {
        private TouchControllerCharacter m_Character;


        private void Awake()
        {
            m_Character = GetComponent<TouchControllerCharacter>();
        }

        private void FixedUpdate()
        {

            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            float v = CrossPlatformInputManager.GetAxis("Vertical");
            // Pass all parameters to the character control script.       
            m_Character.Move(h, v);
        }
    }
}
