﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonController : MonoBehaviour {

    public GameObject SettingsScreen;
    public GameObject InteractionPanel;
    public GameObject InventoryPanel;
    public GameObject CharacterPanel;
    public GameObject SkillPanel;

    // Use this for initialization
    void Start () {
        SettingsScreen.SetActive(false);
        InteractionPanel.SetActive(false);
        InventoryPanel.SetActive(false);
        CharacterPanel.SetActive(false);
        SkillPanel.SetActive(false);


    }

    // Update is called once per frame
    void Update () {
		
	}

    public void SettingsButtonClick()
    {
        Debug.Log("SettingsButtonClicked");
        SettingsScreen.SetActive(true);
    }

    public void SettingsCloseButtonClick()
    {
        Debug.Log("SettingsCloseButtonClick");
        SettingsScreen.SetActive(false);
    }
    public void InteractionButtonClick()
    {
        Debug.Log("InteractionButtonClick");
        InteractionPanel.SetActive(true);
    }
    public void InteractionCloseButtonClick()
    {
        Debug.Log("InteractionCloseButtonClick");
        InteractionPanel.SetActive(false);
    }
    public void CharacterButtonClick()
    {
        Debug.Log("CharacterButtonClick");
        CharacterPanel.SetActive(true);
    }
    public void CharacterCloseButtonClick()
    {
        Debug.Log("CharacterCloseButtonClick");
        CharacterPanel.SetActive(false);
    }
    public void SkillButtonClick()
    {
        Debug.Log("SkillButtonClick");
        SkillPanel.SetActive(true);
    }
    public void SkillCloseButtonClick()
    {
        Debug.Log("SkillCloseButtonClick");
        SkillPanel.SetActive(false);
    }
    public void InventoryButtonClick()
    {
        Debug.Log("InventoryButtonClick");
        InventoryPanel.SetActive(true);
    }
    public void InventoryCloseButtonClick()
    {
        Debug.Log("InventoryCloseButtonClick");
        InventoryPanel.SetActive(false);
    }
}
