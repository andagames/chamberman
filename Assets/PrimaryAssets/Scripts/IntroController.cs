﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroController : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        if (hit.collider != null)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Debug.Log("current hit: " + hit.collider.name);
                if (hit.collider.name.Equals("EnterButton"))
                {
                    Debug.Log("Enter Button Clicked");
                    SceneManager.LoadScene("Navigation3");
                }
               

            }
        }
    }
}