﻿using UnityEngine;
using System.Collections;

public class FromRightController : Creature {

    public GameObject slowedVersion;
    public GameObject freezedVersion;

    void Start () {
        MoveSpeedX = Constants.FIXED_SPEED_NEGATIVE;
        MoveSpeedY = Numbers.ZERO;

        myRigidBody = GetComponent<Rigidbody2D>();
        float x = UnityEngine.Random.Range(10, 15);
        float y = UnityEngine.Random.Range(-4, 4);
        myRigidBody.transform.position = new Vector2(x, y);
	}

	// Update is called once per frame
	void Update () {

        base.checkAffectedSpeed();
        base.checkOutOfScene();
        base.checkIsCollidedWithFireCircle();
        base.checkIsSlowerObjectsClicked();

        checkSpeed();
        checkFreezeJoker();
        checkSlowerJoker();
    }

    public void checkSpeed()
    {
        if (!SessionParameters.isStopGameClicked && !SessionParameters.isGameOver && !SessionParameters.isFreezeThemAllClicked)
        {
            if (isCollidedWithDivineShield)
            {
                MoveSpeedX = Constants.FIXED_SPEED_POSITIVE * Constants.OPPOSITE_DIRECTION_MULTIPLIER;
                MoveSpeedY = Numbers.ZERO;
                GetComponent<SpriteRenderer>().flipX = true;
            }
            else
            {
                MoveSpeedX = Constants.FIXED_SPEED_NEGATIVE;
                MoveSpeedY = Numbers.ZERO;
            }
        }
    }

    public void checkFreezeJoker()
    {
        if (SessionParameters.isFreezeThemAllClicked)
        {
            freezedVersion.SetActive(true);
        }
        else
        {
            freezedVersion.SetActive(false);
        }
    }

    public void checkSlowerJoker()
    {
        if (SessionParameters.isSlowerObjectsClicked)
        {
            slowedVersion.SetActive(true);
        }
        else
        {
            slowedVersion.SetActive(false);
        }
    }
}
