﻿using UnityEngine;
using System.Collections;

public class FromLeftDownCrossController : Creature
{
    public GameObject slowedVersion;
    public GameObject freezedVersion;

    void Start()
    {
        MoveSpeedX = Constants.FIXED_SPEED_POSITIVE;
        MoveSpeedY = Constants.FIXED_SPEED_POSITIVE;

        myRigidBody = GetComponent<Rigidbody2D>();
        float x = UnityEngine.Random.Range(-15, -5);
        float y = UnityEngine.Random.Range(-10, 0);
        myRigidBody.transform.position = new Vector2(x, y);
    }
    
    void Update()
    {
        base.checkAffectedSpeed();
        base.checkOutOfScene();
        base.checkIsCollidedWithFireCircle();
        base.checkIsSlowerObjectsClicked();

        checkSpeed();
        checkFreezeJoker();
        checkSlowerJoker();
    }

    public void checkSpeed()
    {
        if (!SessionParameters.isStopGameClicked && !SessionParameters.isGameOver && !SessionParameters.isFreezeThemAllClicked)
        {
            if (isCollidedWithDivineShield)
            {
                MoveSpeedX = Constants.FIXED_SPEED_NEGATIVE * Constants.OPPOSITE_DIRECTION_MULTIPLIER;
                MoveSpeedY = Constants.FIXED_SPEED_NEGATIVE * Constants.OPPOSITE_DIRECTION_MULTIPLIER;
                GetComponent<SpriteRenderer>().flipX = true;
                GetComponent<SpriteRenderer>().flipY = true;
            }
            else {
                MoveSpeedX = Constants.FIXED_SPEED_POSITIVE;
                MoveSpeedY = Constants.FIXED_SPEED_POSITIVE;
            }
        }
    }

    public void checkFreezeJoker()
    {
        if (SessionParameters.isFreezeThemAllClicked)
        {
            freezedVersion.SetActive(true);
        }
        else
        {
            freezedVersion.SetActive(false);
        }
    }

    public void checkSlowerJoker()
    {
        if (SessionParameters.isSlowerObjectsClicked)
        {
            slowedVersion.SetActive(true);
        }
        else
        {
            slowedVersion.SetActive(false);
        }
    }
}
