﻿using UnityEngine;
using System.Collections;

public class Level3bController : Creature
{
    public IEnumerator bomberCoroutine;

    public GameObject slowedVersion;
    public GameObject freezedVersion;
    public GameObject level3bBomber;

    void Start()
    {
        MoveSpeedX = Constants.FIXED_SPEED_POSITIVE;
        MoveSpeedY = Numbers.ZERO;

        myRigidBody = GetComponent<Rigidbody2D>();
        float x = -10f;
        float y = -3.5f;
        myRigidBody.transform.position = new Vector2(x, y);

        // let's start counting the time
        bomberCoroutine = StartBomber();
        StartCoroutine(bomberCoroutine);
    }

    void Update()
    {
        base.checkAffectedSpeed();
        base.checkOutOfScene();
        base.checkIsCollidedWithFireCircle();
        base.checkIsSlowerObjectsClicked();

        checkSpeed();
        checkFreezeJoker();
        checkSlowerJoker();
    }

    public void checkSpeed()
    {
        if (!SessionParameters.isStopGameClicked && !SessionParameters.isGameOver && !SessionParameters.isFreezeThemAllClicked)
        {
            if (isCollidedWithDivineShield)
            {
                MoveSpeedX = Constants.FIXED_SPEED_NEGATIVE * Constants.OPPOSITE_DIRECTION_MULTIPLIER;
                MoveSpeedY = Numbers.ZERO;
                GetComponent<SpriteRenderer>().flipX = true;
            }
            else
            {
                MoveSpeedX = Constants.FIXED_SPEED_POSITIVE;
                MoveSpeedY = Numbers.ZERO;
            }
        }
    }

    IEnumerator StartBomber()
    {
        while (true)
        {
            yield return new WaitForSeconds(2);

            if (!SessionParameters.isStopGameClicked && !SessionParameters.isGameOver && !SessionParameters.isFreezeThemAllClicked)
            {
                GameObject creature = Instantiate(level3bBomber) as GameObject;
                creature.GetComponent<Rigidbody2D>().transform.position =
                    new Vector2(myRigidBody.transform.position.x, myRigidBody.transform.position.y);
                creature.GetComponent<Rigidbody2D>().velocity = new Vector2();
            }
        }
    }

    public void checkFreezeJoker()
    {
        if (SessionParameters.isFreezeThemAllClicked)
        {
            freezedVersion.SetActive(true);
        }
        else
        {
            freezedVersion.SetActive(false);
        }
    }

    public void checkSlowerJoker()
    {
        if (SessionParameters.isSlowerObjectsClicked)
        {
            slowedVersion.SetActive(true);
        }
        else
        {
            slowedVersion.SetActive(false);
        }
    }
}
