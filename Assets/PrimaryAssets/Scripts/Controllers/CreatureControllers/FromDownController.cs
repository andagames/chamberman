﻿using UnityEngine;
using System.Collections;

public class FromDownController : Creature
{
    public GameObject slowedVersion;
    public GameObject freezedVersion;

    void Start()
    {
        MoveSpeedX = Numbers.ZERO;
        MoveSpeedY = Constants.FIXED_SPEED_POSITIVE;

        myRigidBody = GetComponent<Rigidbody2D>();
        float x = UnityEngine.Random.Range(-7, 7); 
        float y = UnityEngine.Random.Range(-10, -5);
        myRigidBody.transform.position = new Vector2(x, y);
    }

    void Update()
    {
        base.checkAffectedSpeed();
        base.checkOutOfScene();
        base.checkIsCollidedWithFireCircle();
        base.checkIsSlowerObjectsClicked();
        
        checkSpeed();
        checkFreezeJoker();
        checkSlowerJoker();
    }

    public void checkSpeed() {

        if (!SessionParameters.isStopGameClicked && !SessionParameters.isGameOver && !SessionParameters.isFreezeThemAllClicked)
        {
            if (isCollidedWithDivineShield)
            {
                MoveSpeedX = Numbers.ZERO;
                MoveSpeedY = Constants.FIXED_SPEED_NEGATIVE * Constants.OPPOSITE_DIRECTION_MULTIPLIER;
                GetComponent<SpriteRenderer>().flipY = true;
            }
            else
            {
                MoveSpeedX = Numbers.ZERO;
                MoveSpeedY = Constants.FIXED_SPEED_POSITIVE;
            }
        }
    }

    public void checkFreezeJoker() {
        if (SessionParameters.isFreezeThemAllClicked) {
            freezedVersion.SetActive(true);
        }
        else {
            freezedVersion.SetActive(false);
        }
    }

    public void checkSlowerJoker() {
        if (SessionParameters.isSlowerObjectsClicked)
        {
            slowedVersion.SetActive(true);
        }
        else
        {
            slowedVersion.SetActive(false);
        }
    }
}
