﻿using UnityEngine;

public class GameOverController : MonoBehaviour
{
    public GameObject GameOverPanel;
    public GameObject Yes;
    public GameObject No;

    public GameObject Cember;
    public GameObject Hero;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        if (hit.collider != null)
        {
            if (Input.GetMouseButtonDown(0))
            {
                checkColliders(hit.collider.name);
            }
        }

    }

    public void checkColliders(string touchedObjectName)
    {
        if (touchedObjectName.Equals(Buttons.YES_BUTTON))
        {
            GameOverPanel.SetActive(false);
            SessionParameters.isGameOver = false;
            
            // çemberin dışına çıkarak bitti
            if (SessionParameters.isOutOfCember) {
                Creature.destroyAllCreatures();
                Hero.GetComponent<Rigidbody2D>().position =
                    new Vector2(Cember.GetComponent<Rigidbody2D>().position.x, Cember.GetComponent<Rigidbody2D>().position.y);
            }
            // enerjisi bitmişse
            else if (SessionParameters.isEnergyLessThanZero) {
                Creature.destroyAllCreatures();
                Hero.GetComponent<Rigidbody2D>().position =
                    new Vector2(Cember.GetComponent<Rigidbody2D>().position.x, Cember.GetComponent<Rigidbody2D>().position.y);
            }
            // canı bitmişse
            else if (SessionParameters.areLivesLessThanZero)
            {
                Creature.destroyAllCreatures();
                Hero.GetComponent<Rigidbody2D>().position =
                    new Vector2(Cember.GetComponent<Rigidbody2D>().position.x, Cember.GetComponent<Rigidbody2D>().position.y);
            }
            GameMode12Controller.totalSecondCount = Constants.INITIAL_TIME;
            HeroController.makePropertiesInitial();
        }
        else if (touchedObjectName.Equals(Buttons.NO_BUTTON))
        {
            GameOverPanel.SetActive(false);
        }
    }
}