﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class JokerController : MonoBehaviour
{
    public GameObject GameOverPanel;

    private IEnumerator killThemAllTimeCoroutine;
    public GameObject killThemAllTimeText;

    private IEnumerator cemberFireTimeCoroutine;
    public GameObject cemberFireTimeText;

    private IEnumerator divineShieldTimeCoroutine;
    public GameObject divineShieldTimeText;

    private IEnumerator biggerCircleTimeCoroutine;
    public GameObject biggerCircleTimeText;

    private IEnumerator slowerCreaturesTimeCoroutine;
    public GameObject slowerCreaturesTimeText;

    private IEnumerator smallerHeroTimeCoroutine;
    public GameObject smallerHeroTimeText;

    private IEnumerator fasterTimeCoroutine;
    public GameObject fasterTimeText;

    private IEnumerator freezeTimeCoroutine;
    public GameObject FreezeTimeText;
    
    public GameObject Fire;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        if (hit.collider != null)
        {
            if (Input.GetMouseButtonDown(0))
            {
                // oyun bitmemişse, istenilen joker yada yön tuşu çalışır
                if (!SessionParameters.isGameOver && !SessionParameters.isStopGameClicked)
                {
                    checkColliders(hit.collider.name);
                }
            }
        }
        checkIsGameOver();
    }

    public void checkColliders(string touchedObjectName)
    {
        if (touchedObjectName.Equals(Buttons.START_EVERYTHING_AGAIN))
        {
            SessionParameters.isGameOver = true;
            SessionParameters.isOutOfCember = true;
            SessionParameters.isEnergyLessThanZero = true;
            GameOverPanel.SetActive(true);

        }
        else if (touchedObjectName.Equals(Buttons.CEMBER_FIRE_BUTTON))
        {
            if(SessionParameters.isCemberFireClicked == false) {
                SessionParameters.isCemberFireClicked = true;
                cemberFireTimeCoroutine = showCemberFireTimeAnimation();
                StartCoroutine(cemberFireTimeCoroutine);
                Debug.Log(Buttons.CEMBER_FIRE_BUTTON);
            }
        }
        else if (touchedObjectName.Equals(Buttons.DIVINE_SHIELD_BUTTON))
        {
            if (SessionParameters.isDivineShieldClicked == false) {
                SessionParameters.isDivineShieldClicked = true;
                divineShieldTimeCoroutine = showDivineShieldTimeAnimation();
                StartCoroutine(divineShieldTimeCoroutine);
                Debug.Log(Buttons.DIVINE_SHIELD_BUTTON);
            }
        }
        else if (touchedObjectName.Equals(Buttons.BIGGER_CIRCLE_BUTTON))
        {
            if (SessionParameters.isBiggerCircleClicked == false) {
                SessionParameters.isBiggerCircleClicked = true;
                biggerCircleTimeCoroutine = showBiggerCircleTimeAnimation();
                StartCoroutine(biggerCircleTimeCoroutine);
                Debug.Log(Buttons.BIGGER_CIRCLE_BUTTON);
            }
        }
        else if (touchedObjectName.Equals(Buttons.SLOWER_CREATURES_BUTTON))
        {
            if (SessionParameters.isSlowerObjectsClicked == false) {
                SessionParameters.isSlowerObjectsClicked = true;
                slowerCreaturesTimeCoroutine = showSlowerCreaturesTimeAnimation();
                StartCoroutine(slowerCreaturesTimeCoroutine);
                Debug.Log(Buttons.SLOWER_CREATURES_BUTTON);
            }
        }
        else if (touchedObjectName.Equals(Buttons.SMALLER_HERO_BUTTON))
        {
            if (SessionParameters.isSmallerHeroClicked == false) {
                SessionParameters.isSmallerHeroClicked = true;
                smallerHeroTimeCoroutine = showSmallerHeroTimeAnimation();
                StartCoroutine(smallerHeroTimeCoroutine);
                Debug.Log(Buttons.SMALLER_HERO_BUTTON);
            }
        }
        else if (touchedObjectName.Equals(Buttons.FASTER_BUTTON))
        {
            if (SessionParameters.isFasterHeroClicked == false) {
                SessionParameters.isFasterHeroClicked = true;
                fasterTimeCoroutine = showFasterTimeAnimation();
                StartCoroutine(fasterTimeCoroutine);
                Debug.Log(Buttons.FASTER_BUTTON);
            }
        }
        else if (touchedObjectName.Equals(Buttons.FREEZE_BUTTON))
        {
            if (SessionParameters.isFreezeThemAllClicked == false) {
                SessionParameters.isFreezeThemAllClicked = true;
                freezeTimeCoroutine = showStopTimeAnimation();
                StartCoroutine(freezeTimeCoroutine);
                Debug.Log(Buttons.FREEZE_BUTTON);
            }
        }
        else if (touchedObjectName.Equals(Buttons.FIRE_BUTTON))
        {
            Debug.Log(Buttons.FIRE_BUTTON);
            GameObject creature = Instantiate(Fire) as GameObject;
        }
    }

    IEnumerator showKillThemAllTimeAnimation()
    {
        killThemAllTimeText.SetActive(true);

        int i = Constants.JOKER_TIME;

        while (true) {
            killThemAllTimeText.GetComponent<TextMeshPro>().text = i.ToString();

            yield return new WaitForSeconds(1);

            if (!SessionParameters.isStopGameClicked) { 
                i--;
            } 
            if (i == 0)
            {
                break;
            }
        }
        killThemAllTimeText.SetActive(false);
        SessionParameters.isKillThemAllClicked = false;
    }

    IEnumerator showCemberFireTimeAnimation()
    {
        cemberFireTimeText.SetActive(true);

        int i = Constants.JOKER_TIME;

        while (true)
        {
            cemberFireTimeText.GetComponent<TextMeshPro>().text = i.ToString();

            yield return new WaitForSeconds(1);

            if (!SessionParameters.isStopGameClicked)
            {
                i--;
            }
            if (i == 0)
            {
                break;
            }
        }
        cemberFireTimeText.SetActive(false);
        SessionParameters.isCemberFireClicked= false;
    }

    IEnumerator showDivineShieldTimeAnimation()
    {
        divineShieldTimeText.SetActive(true);

        int i = Constants.JOKER_TIME;

        while (true)
        {
            divineShieldTimeText.GetComponent<TextMeshPro>().text = i.ToString();

            yield return new WaitForSeconds(1);

            if (!SessionParameters.isStopGameClicked)
            {
                i--;
            }
            if (i == 0)
            {
                break;
            }
        }
        divineShieldTimeText.SetActive(false);
        SessionParameters.isDivineShieldClicked= false;
    }

    IEnumerator showBiggerCircleTimeAnimation()
    {
        biggerCircleTimeText.SetActive(true);

        int i = Constants.JOKER_TIME;

        while (true)
        {
            biggerCircleTimeText.GetComponent<TextMeshPro>().text = i.ToString();

            yield return new WaitForSeconds(1);

            if (!SessionParameters.isStopGameClicked)
            {
                i--;
            }
            if (i == 0)
            {
                break;
            }
        }
        biggerCircleTimeText.SetActive(false);
        SessionParameters.isBiggerCircleClicked = false;
    }

    IEnumerator showSlowerCreaturesTimeAnimation()
    {
        slowerCreaturesTimeText.SetActive(true);

        int i = Constants.JOKER_TIME;

        while (true)
        {
            slowerCreaturesTimeText.GetComponent<TextMeshPro>().text = i.ToString();

            yield return new WaitForSeconds(1);

            if (!SessionParameters.isStopGameClicked)
            {
                i--;
            }
            if (i == 0)
            {
                break;
            }
        }
        slowerCreaturesTimeText.SetActive(false);
        SessionParameters.isSlowerObjectsClicked = false;
    }

    IEnumerator showSmallerHeroTimeAnimation()
    {
        smallerHeroTimeText.SetActive(true);

        int i = Constants.JOKER_TIME;

        while (true)
        {
            smallerHeroTimeText.GetComponent<TextMeshPro>().text = i.ToString();

            yield return new WaitForSeconds(1);

            if (!SessionParameters.isStopGameClicked)
            {
                i--;
            }
            if (i == 0)
            {
                break;
            }
        }
        smallerHeroTimeText.SetActive(false);
        SessionParameters.isSmallerHeroClicked = false;
    }

    IEnumerator showFasterTimeAnimation()
    {
        fasterTimeText.SetActive(true);

        int i = Constants.JOKER_TIME;

        while (true)
        {
            fasterTimeText.GetComponent<TextMeshPro>().text = i.ToString();

            yield return new WaitForSeconds(1);

            if (!SessionParameters.isStopGameClicked)
            {
                i--;
            }
            if (i == 0)
            {
                break;
            }
        }
        fasterTimeText.SetActive(false);
        SessionParameters.isFasterHeroClicked = false;
    }

    IEnumerator showStopTimeAnimation()
    {
        FreezeTimeText.SetActive(true);

        int i = Constants.JOKER_TIME;

        while (true)
        {
            FreezeTimeText.GetComponent<TextMeshPro>().text = i.ToString();

            yield return new WaitForSeconds(1);

            if (!SessionParameters.isStopGameClicked)
            {
                i--;
            }
            if (i == 0)
            {
                break;
            }
        }
        FreezeTimeText.SetActive(false);
        SessionParameters.isFreezeThemAllClicked = false;
    }

    private void checkIsGameOver()
    {
        if (SessionParameters.isGameOver) {
            SessionParameters.makeEveryJokerInactive();
            SessionParameters.makeGameSpeedInitial();
            StopAllCoroutines();
            setJokerTexts(false);
        }
    }

    private void setJokerTexts(bool visibility)
    {
        cemberFireTimeText.SetActive(visibility);
        divineShieldTimeText.SetActive(visibility);
        biggerCircleTimeText.SetActive(visibility);
        slowerCreaturesTimeText.SetActive(visibility);
        smallerHeroTimeText.SetActive(visibility);
        fasterTimeText.SetActive(visibility);
        FreezeTimeText.SetActive(visibility);
    }

    private void setJokerTextsExceptFreezeTime(bool visibility)
    {
        cemberFireTimeText.SetActive(visibility);
        divineShieldTimeText.SetActive(visibility);
        biggerCircleTimeText.SetActive(visibility);
        slowerCreaturesTimeText.SetActive(visibility);
        smallerHeroTimeText.SetActive(visibility);
        fasterTimeText.SetActive(visibility);
    }
}