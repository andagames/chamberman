﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class GameMode12Controller : MonoBehaviour
{
    private IEnumerator creaatureCoroutine;
    private IEnumerator timerCoroutine;
    //
    public static int totalSecondCount = Numbers.ZERO;
    public static int second = 0;
    public static int minute = 0;
    //
    public GameObject SecondText;
    // public GameObject MinuteText;

    // Use this for initialization
    void Start()
    {
        // let's start creating the creatures
        creaatureCoroutine = GenerateCreatures();
        StartCoroutine(creaatureCoroutine);
        
        // let's start counting the time
        timerCoroutine = StartTimer();
        StartCoroutine(timerCoroutine);

    }

    // Update is called once per frame
    void Update()
    {
        controlTextsInScene();
    }

    public void controlTextsInScene()
    {
        second = totalSecondCount % Constants.SECS_PER_MIN;
        minute = totalSecondCount / Constants.SECS_PER_MIN;

        // eğer saniye 10'dan küçükse, başına 0 göstererek konulur
        if (second < Numbers.TEN)
        {
            SecondText.GetComponent<TextMeshPro>().text = Numbers.ZERO.ToString() + second.ToString(); ;
        }
        else
        {
            SecondText.GetComponent<TextMeshPro>().text = second.ToString();
        }

        // // eğer dakika 10'dan küçükse, başına 0 göstererek konulur
        if (minute < Numbers.TEN)
        {
            // MinuteText.GetComponent<TextMeshPro>().text = Numbers.ZERO.ToString() + minute.ToString(); ;
        }
        else
        {
            // MinuteText.GetComponent<TextMeshPro>().text = second.ToString();
        }
    }
    
    IEnumerator StartTimer()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);

            if (!SessionParameters.isStopGameClicked && !SessionParameters.isGameOver)
            {
                totalSecondCount += Numbers.ONE;
                HeroController.energy -= Numbers.TEN;
//                if (totalSecondCount == Numbers.ZERO)
//                {
//                    SessionParameters.isGameOver = true;
//                }
            }
        }
    }

    IEnumerator GenerateCreatures()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);

            if (!SessionParameters.isStopGameClicked && !SessionParameters.isGameOver && !SessionParameters.isFreezeThemAllClicked)
            {
                createArrangedLevels();
            }
        }
    }

    private void createArrangedLevels()
    {
        if (HeroController.distance > Numbers.ZERO)
        {
            if (HeroController.distance % Numbers.FIFTEEN == Numbers.ZERO)
            {
                LevelController.createEnergy();
            }
            if (HeroController.distance % Numbers.TWENTY == Numbers.ZERO)
            {
                int randomLevel = Numbers.ZERO;

                randomLevel = UnityEngine.Random.Range(0, Constants.TOTAL_NUMBER_OF_LEVELS); // rastgele bir level seç
                Debug.Log("random level: " + randomLevel);

                LevelController.createLevel(randomLevel);
            }
        }
    }
}