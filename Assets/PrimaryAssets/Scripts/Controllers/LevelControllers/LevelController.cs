﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelController : MonoBehaviour {

    public GameObject fromRight;
    public GameObject fromDown;
    public GameObject fromLeft;
    public GameObject fromUp;
    public GameObject fromRightUpCross;
    public GameObject fromRightDownCross;
    public GameObject fromLeftDownCross;
    public GameObject fromLeftUpCross;
    //
    public GameObject level3b;
    public GameObject level3d;
    //
    public GameObject oilDrop;
    //
    public static List<GameObject> level0creatureList = new List<GameObject>();
    public static List<GameObject> level3creatureList = new List<GameObject>();
    public static List<GameObject> level4creatureList = new List<GameObject>();
    //

    void Start()
    {
        level0creatureList.Add(fromRight);
        level0creatureList.Add(fromDown);
        level0creatureList.Add(fromLeft);
        level0creatureList.Add(fromUp);
        level0creatureList.Add(fromRightUpCross);
        level0creatureList.Add(fromRightDownCross);
        level0creatureList.Add(fromLeftDownCross);
        level0creatureList.Add(fromLeftUpCross);
        //
        level3creatureList.Add(level3b);
        level3creatureList.Add(level3d);
        //
        level4creatureList.Add(oilDrop);
    }
    public static void createLevel(int randomLevel)
    {
        if (randomLevel == 0)
        {
            createLevel0();
        }
        else if (randomLevel == 1)
        {
            createLevel1();
        }
        else if (randomLevel == 2)
        {
            createLevel2();
        } 
        else if (randomLevel == 3)
        {
            createLevel3();
        }
    }
    // herhangi bir yönden tek gelme durumu ->
    private static void createLevel0() {
        int randomDirection = UnityEngine.Random.Range(Numbers.ZERO, Constants.TOTAL_NUMBER_OF_DIRECTIONS);
        GameObject creature = Instantiate(level0creatureList[randomDirection]) as GameObject;
    }

    // aynı yönden 2 tane gelme durumu -> -> 
    private static void createLevel1() {
        int randomDirection = UnityEngine.Random.Range(Numbers.ZERO, Constants.TOTAL_NUMBER_OF_DIRECTIONS);
        GameObject creatureOne = Instantiate(level0creatureList[randomDirection]) as GameObject;
        GameObject creatureTwo = Instantiate(level0creatureList[randomDirection]) as GameObject;
    }

    // farklı yönlerden 2 tane gelme durumu -> <-
    private static void createLevel2() {
        for (int i = Numbers.ZERO; i < Numbers.TWO; i++) {
            int randomDirection = UnityEngine.Random.Range(Numbers.ZERO, Constants.TOTAL_NUMBER_OF_DIRECTIONS);
            GameObject creature = Instantiate(level0creatureList[randomDirection]) as GameObject;
        }
    }

    // yukarıdan veya aşağıdan gelip bomba atma
    private static void createLevel3() {
        for (int i = Numbers.ZERO; i < Numbers.ONE; i++) {
            int randomDirection = UnityEngine.Random.Range(Numbers.ZERO, 2);
            GameObject creature = Instantiate(level3creatureList[randomDirection]) as GameObject;
        }
    }

    // yağ parçacıkları
    public static void createEnergy() {
        for (int i = Numbers.ZERO; i < Numbers.ONE; i++) {
            GameObject creature = Instantiate(level4creatureList[Numbers.ZERO]) as GameObject;
        }
    }
}
