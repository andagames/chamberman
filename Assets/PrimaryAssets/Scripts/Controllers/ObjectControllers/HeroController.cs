﻿using TMPro;
using UnityEngine;

public class HeroController : MonoBehaviour
{
    private Rigidbody2D heroRigidBody;
    private Rigidbody2D shieldRigidBody;

    public float MoveSpeedX = 0;
    public float MoveSpeedY = 0;
    public static float speedMultiplier = 1f;
    //
    public GameObject divineShield;
    public GameObject GameOverPanel;

    public GameObject EnergyText;
    public GameObject LivesText;
    //
    public static int energy = Constants.INITAL_ENERGY;
    public static int lives = Constants.INITAL_LIVES;
    public static int distance = Numbers.ZERO;

    public bool isCollidedWithEnergy = false;

    // Use this for initialization
    void Start()
    {
        heroRigidBody = GetComponent<Rigidbody2D>();
        shieldRigidBody = divineShield.GetComponent<Rigidbody2D>();

        divineShield.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        checkSmallerHeroJoker();
        controlDirections();
        showDivineShieldIfExisted();
        controlEnergy();
        checkIsCollidedWithEnergy();
        savePropertiesIntoPlayerPrefs();
        
        EnergyText.GetComponent<TextMeshPro>().text = energy.ToString();
        LivesText.GetComponent<TextMeshPro>().text = lives.ToString();
    }

    public void controlEnergy()
    {
        if(energy <= Numbers.ZERO)
        {
            SessionParameters.isGameOver = true;
            SessionParameters.isEnergyLessThanZero = true;
            GameOverPanel.SetActive(true);
        }
    }

    public void checkSmallerHeroJoker()
    {
        if (SessionParameters.isSmallerHeroClicked)
        {
            float heroX = Constants.HERO_FIXED_SCALE_X / 1.5f;
            float heroY = Constants.HERO_FIXED_SCALE_Y / 1.5f;
            float heroZ = Constants.HERO_FIXED_SCALE_Z / 1.5f;
            heroRigidBody.transform.localScale = new Vector3(heroX, heroY, heroZ);

            float shieldX = Constants.SHIELD_FIXED_SCALE_X / 1.2f;
            float shieldY = Constants.SHIELD_FIXED_SCALE_Y / 1.2f;
            float shieldZ = Constants.SHIELD_FIXED_SCALE_Z / 1.2f;
            shieldRigidBody.transform.localScale = new Vector3(shieldX, shieldY, shieldZ);             
        } else {

            heroRigidBody.transform.localScale
                = new Vector3(Constants.HERO_FIXED_SCALE_X, Constants.HERO_FIXED_SCALE_Y, Constants.HERO_FIXED_SCALE_Z);

            shieldRigidBody.transform.localScale =
                new Vector3(Constants.SHIELD_FIXED_SCALE_X, Constants.SHIELD_FIXED_SCALE_Y, Constants.SHIELD_FIXED_SCALE_Z);
        }
    }

    public void controlDirections()
    {
        if (SessionParameters.isStopGameClicked || SessionParameters.isGameOver)
        {
            MoveSpeedX = Numbers.ZERO;
            MoveSpeedY = Numbers.ZERO;
        }
        else
        {
            float xLocation = heroRigidBody.position.x;
            float yLocation = heroRigidBody.position.y;

            if (SessionParameters.isFasterHeroClicked)
            {
                speedMultiplier = Constants.HERO_SPEED_MULTIPLIER;
                SessionParameters.isFasterHeroClicked = false;
            }

            if (HeroDirectionController.isRightButtonClicked)
            {
                if(canGoToRight(xLocation)) { 
                    MoveSpeedX = Constants.FIXED_HERO_SPEED_POSITIVE * speedMultiplier;
                    MoveSpeedY = Numbers.ZERO;
                    Debug.Log("SessionParameters.isFasterHeroClicked: " + SessionParameters.isFasterHeroClicked);

                    if (SessionParameters.isFasterHeroClicked) { 
                        HeroController.distance += Numbers.FIFTEEN;
                    } else
                    {
                        HeroController.distance += Numbers.TEN;
                    }
                    Debug.Log("Distance: " + HeroController.distance);
                }
                HeroDirectionController.isRightButtonClicked = false;
            }
            else if (HeroDirectionController.isDownButtonClicked)
            {
                if (canGoToDown(yLocation)) { 
                    MoveSpeedX = Numbers.ZERO;
                    MoveSpeedY = Constants.FIXED_HERO_SPEED_NEGATIVE * speedMultiplier;
                }
                HeroDirectionController.isDownButtonClicked = false;
            }
            else if (HeroDirectionController.isLeftButtonClicked)
            {
                if(canGoToLeft(xLocation)) {
                    MoveSpeedX = Constants.FIXED_HERO_SPEED_NEGATIVE * speedMultiplier;
                    MoveSpeedY = Numbers.ZERO;

                    Debug.Log("SessionParameters.isFasterHeroClicked: " + SessionParameters.isFasterHeroClicked);
                    if (SessionParameters.isFasterHeroClicked)
                    {
                        HeroController.distance -= Numbers.FIFTEEN;
                    }
                    else
                    {
                        HeroController.distance -= Numbers.TEN;
                    }
                    Debug.Log("Distance: " + HeroController.distance);
                }

                HeroDirectionController.isLeftButtonClicked = false;
            }
            else if (HeroDirectionController.isUpButtonClicked)
            {
                if(canGoToUp(yLocation)) {
                    MoveSpeedX = Numbers.ZERO;
                    MoveSpeedY = Constants.FIXED_HERO_SPEED_POSITIVE * speedMultiplier;
                }
                HeroDirectionController.isUpButtonClicked = false;
            }
            else
            {
                MoveSpeedX = Numbers.ZERO;
                MoveSpeedY = Numbers.ZERO;
            }
        }
        heroRigidBody.velocity = new Vector2(MoveSpeedX, MoveSpeedY);
        shieldRigidBody.velocity = new Vector2(MoveSpeedX, MoveSpeedY);
    }

    public void showDivineShieldIfExisted()
    {
        if (SessionParameters.isDivineShieldClicked)
        {
            divineShield.SetActive(true);
        }
        else
        {
            divineShield.SetActive(false);
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        // heronun üzerinde divine shield yoksa, canı azalır
        if (collider.gameObject.tag.Equals(Constants.STR_CREATURE))
        {
            if (!SessionParameters.isDivineShieldClicked)
            {
                lives -= Numbers.ONE;
                Destroy(collider.gameObject);

                if (lives <= Numbers.ZERO)
                {
                    SessionParameters.isGameOver = true;
                    SessionParameters.areLivesLessThanZero = true;
                    GameOverPanel.SetActive(true);
                }
            }
        } else if (collider.gameObject.tag.Equals(Constants.STR_ENERGY))
        {
            isCollidedWithEnergy = true;
            Destroy(collider.gameObject);
        }
    }

    public void checkIsCollidedWithEnergy()
    {
        if (isCollidedWithEnergy)
        {
            Debug.Log("Energy ile çarpıstım");
            HeroController.energy += Constants.ENERGY_INCREASE_PER_CATCH;
            isCollidedWithEnergy = false;
        }
        else
        {
        }
    }


    public bool canGoToRight(float xLocation)
    {
        if (xLocation <= Constants.X_MAX_FOR_HERO)
        {
            return true;
        }
        return false;
    }

    public bool canGoToLeft(float xLocation)
    {
        if (xLocation >= Constants.X_MIN_FOR_HERO)
        {
            return true;
        }
        return false;
    }

    public bool canGoToDown(float yLocation)
    {
        if (yLocation >= Constants.Y_MIN_FOR_HERO)
        {
            return true;
        }
        return false;
    }

    public bool canGoToUp(float yLocation)
    {
        if (yLocation <= Constants.Y_MAX_FOR_HERO)
        {
            return true;
        }
        return false;
    }

    public static void makePropertiesInitial()
    {
        energy = Constants.INITAL_ENERGY;
        lives = Constants.INITAL_LIVES;
        distance = Numbers.ZERO;
    }

    private void savePropertiesIntoPlayerPrefs()
    {
        PlayerPrefs.SetInt(PrefStrings.ENERY, energy);
        PlayerPrefs.SetInt(PrefStrings.LIVES, lives);
        PlayerPrefs.SetInt(PrefStrings.DISTANCE, distance);

    }
}