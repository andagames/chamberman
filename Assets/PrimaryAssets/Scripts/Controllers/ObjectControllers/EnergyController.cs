﻿using UnityEngine;

public class EnergyController : MonoBehaviour
{
    private Rigidbody2D oilDropBody;

    private int startTimeSec;

    void Start()
    {
        // sadece enemylerle çarpısma oldugunda, trigger methodunun calısması için
        oilDropBody = GetComponent<Rigidbody2D>();

        float x = UnityEngine.Random.Range(Constants.X_MIN_FOR_HERO, Constants.X_MAX_FOR_HERO);
        float y = UnityEngine.Random.Range(Constants.Y_MIN_FOR_HERO, Constants.Y_MAX_FOR_HERO);
        oilDropBody.transform.position = new Vector2(x, y);

        startTimeSec = GameMode12Controller.totalSecondCount;
    }

    void Update()
    {
        checkIsItTimeToBeDead();
    }

    // belirtilen süre kadar yaşayıp yok olur
    public void checkIsItTimeToBeDead() {

        int liveUntilThatSec = Constants.TIME_TO_BE_ALIVE_FOR_ENERGY + startTimeSec;
        if (liveUntilThatSec <= GameMode12Controller.totalSecondCount) {
            Destroy(this.gameObject);
        }
    }
}