﻿using UnityEngine;
using System.Collections;

public class SmallerCemberController : MonoBehaviour
{
    private Rigidbody2D myRigidBody;
    public GameObject redCircle;

    public static bool isOutOfSmallerCember;

    // Use this for initialization
    void Start()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        checkHeroIsOutOfSmallerCember();
    }

    public void checkHeroIsOutOfSmallerCember()
    {
        if (isOutOfSmallerCember)
        {
            redCircle.SetActive(true);
        } else
        {
            redCircle.SetActive(false);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag.Equals(Constants.STR_PLAYER))
        {
            isOutOfSmallerCember = true;
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag.Equals(Constants.STR_PLAYER))
        {
            isOutOfSmallerCember = false;
        }
    }
}
