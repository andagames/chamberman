﻿using UnityEngine;
using System.Collections;
using TMPro;

public class CemberController : MonoBehaviour
{
    private Rigidbody2D myRigidBody;
    public static float MoveSpeedX;
    public static float MoveSpeedY;
    public static int speedDivisor = 1;
    
    private IEnumerator cemberCoroutine;

    public GameObject GameOverPanel;
    public GameObject SmallerCember;
    public GameObject FireCircle;

    // Use this for initialization
    void Start()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
        myRigidBody.position = new Vector3(0, 0, 20);

        cemberCoroutine = ControlCemberMovement();
        StartCoroutine(cemberCoroutine);
    }

    // Update is called once per frame
    void Update()
    {
        checkIsBiggerCircleJokerClicked();
        checkIsStopTime();
        checkIsSlowerObjectsClicked();
        showFireCircleIfExisted();

        SmallerCember.GetComponent<Rigidbody2D>().velocity = new Vector2(MoveSpeedX / speedDivisor, MoveSpeedY / speedDivisor);
        myRigidBody.velocity = new Vector2(MoveSpeedX / speedDivisor, MoveSpeedY / speedDivisor);
    }

    public void checkIsBiggerCircleJokerClicked()
    {
        if (SessionParameters.isBiggerCircleClicked)
        {
            myRigidBody.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            SmallerCember.GetComponent<Rigidbody2D>().transform.localScale = new Vector3(1.2f, 1.2f, 1);
        }
        else
        {
            myRigidBody.transform.localScale = new Vector3(1f, 1f, 1f);
            SmallerCember.GetComponent<Rigidbody2D>().transform.localScale = new Vector3(0.8f, 0.8f, 1);
        }
    }

    public void checkIsStopTime()
    {
        if (SessionParameters.isStopGameClicked || SessionParameters.isGameOver)
        {
            MoveSpeedX = 0;
            MoveSpeedY = 0;
        }
    }

    public void checkIsSlowerObjectsClicked()
    {
        if (SessionParameters.isSlowerObjectsClicked)
        {
            speedDivisor = 2;
        }
        else
        {
            speedDivisor = 1;
        }
    }

    public void showFireCircleIfExisted()
    {
        if (SessionParameters.isCemberFireClicked)
        {
            FireCircle.SetActive(true);
        }
        else
        {
            FireCircle.SetActive(false);
        }
    }

    IEnumerator ControlCemberMovement()
    {
        int totalDirectionCountToRight = 0;
        int totalDirectionCountToLeft = 0;
        int totalDirectionCountToUp = 0;
        int totalDirectionCountToDown = 0;

        while (true)
        {
            yield return new WaitForSeconds(1f);

            if (!SessionParameters.isStopGameClicked && !SessionParameters.isGameOver)
            {
                int randomDirection = UnityEngine.Random.Range(0, Constants.TOTAL_NUMBER_OF_DIRECTIONS_FOR_CIRCLE);

                // sola, sağdan en az 3 kere fazla gitmiş ve yine sol gelmiş
                if (totalDirectionCountToLeft >= totalDirectionCountToRight + Constants.MAX_DIFFERENCE_BETWEEN_RIGHT_AND_LEFT)
                {
                    if (randomDirection == Constants.LEFT)
                    {
                        randomDirection = Constants.RIGHT;
                    }
                }
                // sağa, soldan en az 3 kere fazla gitmiş ve yine sağ gelmiş
                else if (totalDirectionCountToRight >= totalDirectionCountToLeft + Constants.MAX_DIFFERENCE_BETWEEN_RIGHT_AND_LEFT)
                {
                    if (randomDirection == Constants.RIGHT)
                    {
                        randomDirection = Constants.LEFT;
                    }
                }
                // yukarıya, aşağıdan en az 3 kere fazla gitmiş ve yine yukarı gelmiş
                if (totalDirectionCountToUp >= totalDirectionCountToDown + Constants.MAX_DIFFERENCE_BETWEEN_RIGHT_AND_LEFT)
                {
                    if (randomDirection == Constants.UP)
                    {
                        randomDirection = Constants.DOWN;
                    }
                }
                // aşağıya yukarıdan  en az 3 kere fazla gitmiş ve yine aşağı gelmiş
                else if (totalDirectionCountToDown >= totalDirectionCountToUp + Constants.MAX_DIFFERENCE_BETWEEN_RIGHT_AND_LEFT)
                {
                    if (randomDirection == Constants.DOWN)
                    {
                        randomDirection = Constants.UP;
                    }
                }

                if (randomDirection == Constants.RIGHT)
                {
                    MoveSpeedX = 1;
                    MoveSpeedY = 0;
                    totalDirectionCountToRight += 1;
                }
                else if (randomDirection == Constants.LEFT)
                {
                    MoveSpeedX = -1;
                    MoveSpeedY = 0;
                    totalDirectionCountToLeft += 1;
                }
                else if (randomDirection == Constants.DOWN)
                {
                    MoveSpeedX = 0;
                    MoveSpeedY = 1;
                    totalDirectionCountToDown += 1;
                }
                else if (randomDirection == Constants.UP)
                {
                    MoveSpeedX = 0;
                    MoveSpeedY = -1;
                    totalDirectionCountToUp += 1;
                }
            }
        }
    }

    void OnTriggerExit2D(Collider2D other) {
        // hero çemberin dışına çıkarsa, oyun biter
        if (other.gameObject.tag.Equals(Constants.STR_PLAYER)) {
            SessionParameters.isGameOver = true;
            SessionParameters.isOutOfCember = true;
            GameOverPanel.SetActive(true);
        }
    }
}
