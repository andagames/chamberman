﻿using UnityEngine;
using System.Collections;

public class FireController : MonoBehaviour
{

    private Rigidbody2D fireRigidBody;
    public float MoveSpeedX = 0.1f;
    public float MoveSpeedY = 0;

    public GameObject hero;
    private Rigidbody2D heroRigidBody;

    // Use this for initialization
    void Start()
    {
        hero = GameObject.FindGameObjectWithTag("Player");
        fireRigidBody = GetComponent<Rigidbody2D>();
        heroRigidBody = hero.GetComponent<Rigidbody2D>();

        fireRigidBody.position =
                    new Vector2(hero.GetComponent<Rigidbody2D>().position.x, hero.GetComponent<Rigidbody2D>().position.y);

    }

    // Update is called once per frame
    void Update()
    {
        fireRigidBody.velocity = new Vector2(MoveSpeedX, MoveSpeedY);
    }
}
