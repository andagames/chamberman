﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class GameSpeedController : MonoBehaviour
{

    public GameObject StopGameButton;
    public GameObject FastGameButton;

    public Sprite fast;
    public Sprite normalSpeed;

    public Sprite resume;
    public Sprite stop;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        if (hit.collider != null)
        {
            if (Input.GetMouseButtonDown(0))
            {
                // oyun bitmemişse, oyunu durdurma yada hızlandırma çalışır
                if (!SessionParameters.isGameOver)
                {
                    checkColliders(hit.collider.name);
                }
            }
        }
        checkIsGameOver();
    }

    public void checkColliders(string touchedObjectName)
    {
        // oyunu durdurma kısmı
        if (touchedObjectName.Equals(Buttons.STOP_GAME_BUTTON))
        {
            if (SessionParameters.isStopGameClicked)
            {
                SessionParameters.isStopGameClicked = false;
                StopGameButton.GetComponent<SpriteRenderer>().sprite = stop;
            }
            else
            {
                SessionParameters.isStopGameClicked = true;
                StopGameButton.GetComponent<SpriteRenderer>().sprite = resume;
            }
        }

        // oyunu hızlandırma kısmı
        else if (touchedObjectName.Equals(Buttons.FAST_GAME_BUTTON))
        {
            if (SessionParameters.isFastGameClicked)
            {
                SessionParameters.isFastGameClicked = false;
                FastGameButton.GetComponent<SpriteRenderer>().sprite = fast;
            }
            else
            {
                SessionParameters.isFastGameClicked = true;
                FastGameButton.GetComponent<SpriteRenderer>().sprite = normalSpeed;
            }
        }
    }

    public void checkIsGameOver() { 
        if (SessionParameters.isGameOver) {
            SessionParameters.isStopGameClicked = false;
            SessionParameters.isFastGameClicked = false;
            StopGameButton.GetComponent<SpriteRenderer>().sprite = stop;
            FastGameButton.GetComponent<SpriteRenderer>().sprite = fast;
        }
    }
}
