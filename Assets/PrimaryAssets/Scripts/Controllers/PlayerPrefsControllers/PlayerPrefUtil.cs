﻿using UnityEngine;

class PlayerPrefUtil {

    // AvailableQuestCount
    public static int getAvailableQuestCount() {
        return PlayerPrefs.GetInt(PlayerPrefConstanst.AVAILABLE_QUEST_COUNT);
    }

    public static void setAvailableQuestCount(int availableQuestCount) {
        PlayerPrefs.SetInt(PlayerPrefConstanst.AVAILABLE_QUEST_COUNT, availableQuestCount);
    }

    public static void decreaseAvailableQuestCount(int decreasedQuestCount) {
        int availableQuestCount = PlayerPrefs.GetInt(PlayerPrefConstanst.AVAILABLE_QUEST_COUNT);
        availableQuestCount -= decreasedQuestCount;
        PlayerPrefs.SetInt(PlayerPrefConstanst.AVAILABLE_QUEST_COUNT, availableQuestCount);
    }

    public static void increaseAvailableQuestCount(int increasedQuestCount)
    {
        int availableQuestCount = PlayerPrefs.GetInt(PlayerPrefConstanst.AVAILABLE_QUEST_COUNT);
        availableQuestCount += increasedQuestCount;
        PlayerPrefs.SetInt(PlayerPrefConstanst.AVAILABLE_QUEST_COUNT, availableQuestCount);
    }

    // GoldCount
    public static int getGoldCount() {
        return PlayerPrefs.GetInt(PlayerPrefConstanst.GOLD_COUNT);
    }

    public static void setGoldCount(int goldCount) {
        PlayerPrefs.SetInt(PlayerPrefConstanst.GOLD_COUNT, goldCount);
    }

    public static void decreaseGoldCount(int decreasedGoldCount)
    {
        int availableGoldCount = PlayerPrefs.GetInt(PlayerPrefConstanst.GOLD_COUNT);
        availableGoldCount -= decreasedGoldCount;
        PlayerPrefs.SetInt(PlayerPrefConstanst.GOLD_COUNT, availableGoldCount);
    }

    public static void increaseGoldCount(int increasedGoldCount)
    {
        int availableGoldCount = PlayerPrefs.GetInt(PlayerPrefConstanst.GOLD_COUNT);
        availableGoldCount += increasedGoldCount;
        PlayerPrefs.SetInt(PlayerPrefConstanst.GOLD_COUNT, availableGoldCount);
    }

    // last login hour
    public static int getLastLoginHour() {
        return PlayerPrefs.GetInt(PlayerPrefConstanst.LAST_LOGIN_HOUR);
    }

    public static void setLastLoginHour(int lastLoginDayHour) {
        PlayerPrefs.SetInt(PlayerPrefConstanst.LAST_LOGIN_HOUR, lastLoginDayHour);
    }

    // last login day
    public static int getLastLoginDay() {
        return PlayerPrefs.GetInt(PlayerPrefConstanst.LAST_LOGIN_DAY);
    }

    public static void setLastLoginDay(int lastLoginDay) {
        PlayerPrefs.SetInt(PlayerPrefConstanst.LAST_LOGIN_DAY, lastLoginDay);
    }

    // last login month
    public static int getLastLoginMonth() {
        return PlayerPrefs.GetInt(PlayerPrefConstanst.LAST_LOGIN_MONTH);
    }

    public static void setLastLoginMonth(int lastLoginMonth) {
        PlayerPrefs.SetInt(PlayerPrefConstanst.LAST_LOGIN_MONTH, lastLoginMonth);
    }

    // last login year
    public static int getLastLoginYear() {
        return PlayerPrefs.GetInt(PlayerPrefConstanst.LAST_LOGIN_YEAR);
    }

    public static void setLastLoginYear(int lastLoginYear) {
        PlayerPrefs.SetInt(PlayerPrefConstanst.LAST_LOGIN_YEAR, lastLoginYear);
    }

    // last login date
    public static string getLastLoginDate() {
        return PlayerPrefs.GetString(PlayerPrefConstanst.LAST_LOGIN_DATE);
    }

    public static void setLastLoginDate(string lastLoginDate) {
        PlayerPrefs.SetString(PlayerPrefConstanst.LAST_LOGIN_DATE, lastLoginDate);
    }


    public static void setPilotNumber(int pilotnumber)
    {
        PlayerPrefs.SetInt(PlayerPrefConstanst.PilotNumber, pilotnumber);
    }
    public static int getPilotNumber()
    {
        return PlayerPrefs.GetInt(PlayerPrefConstanst.PilotNumber);
    }

    public static void setBiologNumber(int biolognumber)
    {
        PlayerPrefs.SetInt(PlayerPrefConstanst.BiologNumber, biolognumber);
    }
    public static int getBiologNumber()
    {
        return PlayerPrefs.GetInt(PlayerPrefConstanst.BiologNumber);
    }

    public static void setSoldierNumber(int soldiernumber)
    {
        PlayerPrefs.SetInt(PlayerPrefConstanst.SoldierNumber, soldiernumber);
    }
    public static int getSoldierNumber()
    {
        return PlayerPrefs.GetInt(PlayerPrefConstanst.SoldierNumber);
    }

    public static void setAssasinNumber(int assasinnumber)
    {
        PlayerPrefs.SetInt(PlayerPrefConstanst.AssasinNumber, assasinnumber);
    }
    public static int getAssasinNumber()
    {
        return PlayerPrefs.GetInt(PlayerPrefConstanst.AssasinNumber);
    }

    public static void setDoctorNumber(int doctornumber)
    {
        PlayerPrefs.SetInt(PlayerPrefConstanst.DoctorNumber, doctornumber);
    }
    public static int getDoctorNumber()
    {
        return PlayerPrefs.GetInt(PlayerPrefConstanst.DoctorNumber);
    }



    public static void setSkill1(string skill1)
    {
        PlayerPrefs.SetString(PlayerPrefConstanst.Skill1, skill1);
    }
    public static string getSkill1(string skill1)
    {
        return PlayerPrefs.GetString(PlayerPrefConstanst.Skill1);
    }

    public static void setSkill2(string skill2)
    {
        PlayerPrefs.SetString(PlayerPrefConstanst.Skill2, skill2);
    }
    public static string getSkill2(string skill2)
    {
        return PlayerPrefs.GetString(PlayerPrefConstanst.Skill2);
    }

    public static void setSkill3(string skill3)
    {
        PlayerPrefs.SetString(PlayerPrefConstanst.Skill3, skill3);
    }
    public static string getSkill3(string skill3)
    {
        return PlayerPrefs.GetString(PlayerPrefConstanst.Skill3);
    }



    public static void setElement1(int element1)
    {
        PlayerPrefs.SetInt(PlayerPrefConstanst.Element1, element1);
    }
    public static int getElement1(int element1)
    {
        return PlayerPrefs.GetInt(PlayerPrefConstanst.Element1);
    }

    public static void setElement2(int element2)
    {
        PlayerPrefs.SetInt(PlayerPrefConstanst.Element2, element2);
    }
    public static int getElement2(int element2)
    {
        return PlayerPrefs.GetInt(PlayerPrefConstanst.Element2);
    }

    public static void setElement3(int element3)
    {
        PlayerPrefs.SetInt(PlayerPrefConstanst.Element3, element3);
    }
    public static int getElement3(int element3)
    {
        return PlayerPrefs.GetInt(PlayerPrefConstanst.Element3);
    }

    public static void setElement4(int element4)
    {
        PlayerPrefs.SetInt(PlayerPrefConstanst.Element4, element4);
    }
    public static int getElement4(int element4)
    {
        return PlayerPrefs.GetInt(PlayerPrefConstanst.Element4);
    }

    public static void setElement5(int element5)
    {
        PlayerPrefs.SetInt(PlayerPrefConstanst.Element5, element5);
    }
    public static int getElement5(int element5)
    {
        return PlayerPrefs.GetInt(PlayerPrefConstanst.Element5);
    }

    public static void setElement6(int element6)
    {
        PlayerPrefs.SetInt(PlayerPrefConstanst.Element6, element6);
    }
    public static int getElement6(int element6)
    {
        return PlayerPrefs.GetInt(PlayerPrefConstanst.Element6);
    }

    public static void setElement7(int element7)
    {
        PlayerPrefs.SetInt(PlayerPrefConstanst.Element7, element7);
    }
    public static int getElement7(int element7)
    {
        return PlayerPrefs.GetInt(PlayerPrefConstanst.Element7);
    }

    public static void setElement8(int element8)
    {
        PlayerPrefs.SetInt(PlayerPrefConstanst.Element8, element8);
    }
    public static int getElement8(int element8)
    {
        return PlayerPrefs.GetInt(PlayerPrefConstanst.Element8);
    }
}
