﻿class QuestConstants {

    public static int MAX_NUMBER_OF_AVAILABLE_QUESTS = 3;
    public static int TOTAL_QUEST_COUNT = 4;

    public static string DESTROY_AT_LEAST_FIVE_ROCKETS = "Destroy at least 5 rockets";
    public static string DESTROY_AT_LEAST_FIVE_ALIENS = "Destroy at least 5 aliens";
    public static string STAY_INSIDE_CIRCLE_AT_LEAST_ONE_MINUTE_ = "Stay in circle at least 1 minute";
    public static string FINISH_LEVEL_WITHOUT_USING_ANY_SKILL = "Finish level without using any skill";
}
