﻿using System;

class QuestController {
    public static void arrangeQuests() {
        int availableQuestCount = PlayerPrefUtil.getAvailableQuestCount();
        int maxNumberOfAvailableQuests = QuestConstants.MAX_NUMBER_OF_AVAILABLE_QUESTS;
        
        // listede yeni yaratılabilcek görevler için yer varsa
        if (availableQuestCount < maxNumberOfAvailableQuests) {
            int currentHour = DateTime.Today.Hour;
            int currentDay = DateTime.Today.Day;
            int currentMonth = DateTime.Today.Month;
            int currentYear = DateTime.Today.Year;

            int lastLoginHour = PlayerPrefUtil.getLastLoginHour();
            int lastLoginDay = PlayerPrefUtil.getLastLoginDay();
            int lastLoginMonth = PlayerPrefUtil.getLastLoginMonth();
            int lastLoginYear = PlayerPrefUtil.getLastLoginYear();

            // aynı gün tarihte girdiyse, yeni görev yok
            if(lastLoginDay == currentDay && lastLoginMonth == currentMonth && lastLoginYear == currentYear) {
                    
            }
            // gün farklıysa
            else if (lastLoginDay != currentDay && lastLoginMonth == currentMonth && lastLoginYear == currentYear) {
                int howManyDayPassed = currentDay - lastLoginDay;
                // eğer üzerinden çok gün geçmişse, şimdiki sayıya bakarak en fazla 3 tane yarat
                if(howManyDayPassed >= maxNumberOfAvailableQuests) {
                    createNewQuests(maxNumberOfAvailableQuests - availableQuestCount);
                }
                else {
                    // eğer üzerinden çok gün geçmemişse, 3'ü aşmayacak kadar yarat
                    if (howManyDayPassed + availableQuestCount > maxNumberOfAvailableQuests) {
                        createNewQuests(howManyDayPassed + availableQuestCount - maxNumberOfAvailableQuests); 
                    }
                    // eğer üzerinden çok gün geçmemişse ve 3'ü aşmıyorsa, gün sayısı kadar yarat
                    else
                    {
                        createNewQuests(howManyDayPassed);
                    }
                }
            }
            // eğer üzerinden çok gün geçmişse, şimdiki sayıya bakarak en fazla 3 tane yarat
            else if (lastLoginMonth != currentMonth || lastLoginYear != currentYear) {
                createNewQuests(maxNumberOfAvailableQuests - availableQuestCount);
            }
        } else {
            
        } 
    }

    public static void createNewQuests(int numberOfQuestsToBeCreated) {
        for(int i = 0; i< numberOfQuestsToBeCreated; i++) {
            int randomQuestNumber = UnityEngine.Random.Range(0, QuestConstants.TOTAL_QUEST_COUNT);
        }
    }
}
