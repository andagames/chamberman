﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;


public class HeroDirectionController : MonoBehaviour
{
    public static bool isRightButtonClicked = false;
    public static bool isDownButtonClicked = false;
    public static bool isLeftButtonClicked = false;
    public static bool isUpButtonClicked = false;
    
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        if (hit.collider != null)
        {
            if (Input.GetMouseButtonDown(0))
            {
                // oyun bitmemişse, istenilen joker yada yön tuşu çalışır
                if (!SessionParameters.isGameOver && !SessionParameters.isStopGameClicked)
                {
                    checkColliders(hit.collider.name);
                }
            }
        }
    }

    public void checkColliders(string touchedObjectName)
    {
        if (touchedObjectName.Equals(Buttons.RIGHT_BUTTON))
        {
            isRightButtonClicked = true;
            isDownButtonClicked = false;
            isLeftButtonClicked = false;
            isUpButtonClicked = false;
        }
        else if (touchedObjectName.Equals(Buttons.DOWN_BUTTON))
        {
            isRightButtonClicked = false;
            isDownButtonClicked = true;
            isLeftButtonClicked = false;
            isUpButtonClicked = false;
        }
        else if (touchedObjectName.Equals(Buttons.LEFT_BUTTON))
        {
            isRightButtonClicked = false;
            isDownButtonClicked = false;
            isLeftButtonClicked = true;
            isUpButtonClicked = false;
        }
        else if (touchedObjectName.Equals(Buttons.UP_BUTTON))
        {
            isRightButtonClicked = false;
            isDownButtonClicked = false;
            isLeftButtonClicked = false;
            isUpButtonClicked = true;
        }
    }
}