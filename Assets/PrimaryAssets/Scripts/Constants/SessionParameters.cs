﻿using UnityEngine;

public class SessionParameters : MonoBehaviour
{
    // oyunun durması ve hızlanması
    public static bool isStopGameClicked = false;
    public static bool isFastGameClicked = false;
    // tüm jokerler
    public static bool isKillThemAllClicked = false;
    public static bool isCemberFireClicked = false;
    public static bool isDivineShieldClicked = false;
    public static bool isBiggerCircleClicked = false;
    public static bool isSlowerObjectsClicked = false;
    public static bool isSmallerHeroClicked = false;
    public static bool isFasterHeroClicked = false;
    public static bool isFreezeThemAllClicked = false;
    // oyunun bitmesi ve hangi şekilde bittiği
    public static bool isOutOfCember = false;
    public static bool isEnergyLessThanZero = false;
    public static bool areLivesLessThanZero = false;
    public static bool isGameOver = false;
    //
    public static int myLastLevel = 0;
    //

    public static void makeEveryJokerInactive() {
        isCemberFireClicked = false;
        isDivineShieldClicked = false;
        isBiggerCircleClicked = false;
        isSlowerObjectsClicked = false;
        isSmallerHeroClicked = false;
        isFasterHeroClicked = false;
        isFreezeThemAllClicked = false;
    }

    public static void makeGameSpeedInitial() {
        isStopGameClicked = false;
        isFastGameClicked = false;
    }
}