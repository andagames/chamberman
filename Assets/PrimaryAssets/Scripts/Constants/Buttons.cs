﻿using UnityEngine;
using System.Collections;

public class Buttons : MonoBehaviour
{
    // Directions
    public static string RIGHT_BUTTON = "RightControlButton";
    public static string DOWN_BUTTON = "DownControlButton";
    public static string LEFT_BUTTON = "LeftControlButton";
    public static string UP_BUTTON = "UpControlButton";
    // Stop and fast game
    public static string STOP_GAME_BUTTON = "StopGameButton";
    public static string FAST_GAME_BUTTON = "FastGameButton";
    // Jokers
    public static string START_EVERYTHING_AGAIN = "StartEverythingAgain";
    public static string CEMBER_FIRE_BUTTON = "CemberFireButton";
    public static string DIVINE_SHIELD_BUTTON = "DivineShieldButton";
    public static string BIGGER_CIRCLE_BUTTON = "BiggerCircleButton";
    public static string SLOWER_CREATURES_BUTTON = "SlowerCreaturesButton";
    public static string SMALLER_HERO_BUTTON = "SmallerHeroButton";
    public static string FASTER_BUTTON = "FasterButton";
    public static string FREEZE_BUTTON = "FreezeButton";
    public static string FIRE_BUTTON = "FireButton";
    // game over: yes and no
    public static string YES_BUTTON = "YesButton";
    public static string NO_BUTTON = "NoButton";


}