﻿using UnityEngine;
using System.Collections;

public class Constants : MonoBehaviour
{
    public static int RIGHT = 0;
    public static int DOWN = 1;
    public static int LEFT = 2;
    public static int UP = 3;
    public static int RIGHT_UP = 4;
    public static int RIGHT_DOWN = 5;
    public static int LEFT_DOWN = 6;
    public static int LEFT_UP = 7;

    public static int TOTAL_NUMBER_OF_DIRECTIONS_FOR_CIRCLE = 4;
    public static int TOTAL_NUMBER_OF_DIRECTIONS = 8;
    public static int MAX_DIFFERENCE_BETWEEN_RIGHT_AND_LEFT = 3;
    public static int MAX_DIFFERENCE_BETWEEN_UP_AND_DOWN = 3;

    public static string STR_PLAYER = "Player";
    public static string STR_SHIELD = "Shield";
    public static string STR_FIRE_CIRCLE = "FireCircle";
    public static string STR_CREATURE = "Creature";
    public static string STR_ENERGY = "Energy";
    public static string STR_FROM_RIGHT = "FromRight(Clone)";
    public static string STR_FROM_RIGHT_DOWN = "FromRightDownCross(Clone)";
    public static string STR_FROM_DOWN = "FromDown(Clone)";
    public static string STR_FROM_LEFT_DOWN = "FromLeftDownCross(Clone)";
    public static string STR_FROM_LEFT = "FromLeft(Clone)";
    public static string STR_FROM_LEFT_UP = "FromLeftUpCross(Clone)";
    public static string STR_FROM_UP = "FromUp(Clone)";
    public static string STR_FROM_RIGHT_UP = "FromRightUpCross(Clone)";

    public static float X_MAX_FOR_CREATURE = 15;
    public static float X_MIN_FOR_CREATURE = -15;
    public static float Y_MAX_FOR_CREATURE = 10;
    public static float Y_MIN_FOR_CREATURE = -10;

    public static float X_MAX_FOR_HERO = 7;
    public static float X_MIN_FOR_HERO = -7;
    public static float Y_MAX_FOR_HERO = 4;
    public static float Y_MIN_FOR_HERO = -4;

    public static float HERO_FIXED_SCALE_X = 0.6540241f;
    public static float HERO_FIXED_SCALE_Y = 0.6945147f;
    public static float HERO_FIXED_SCALE_Z = 1f;

    public static float SHIELD_FIXED_SCALE_X = 2f;
    public static float SHIELD_FIXED_SCALE_Y = 2f;
    public static float SHIELD_FIXED_SCALE_Z = 1.25f;

    public static int OPPOSITE_DIRECTION_MULTIPLIER = 2;
    public static int FIXED_SPEED_NEGATIVE = -3;
    public static int FIXED_SPEED_POSITIVE = 3;

    public static float HERO_SPEED_MULTIPLIER = 1.5f;
    public static int FIXED_HERO_SPEED_NEGATIVE = -20;
    public static int FIXED_HERO_SPEED_POSITIVE = 20;

    public static int OIL_SPEED_MULTIPLIER = 2;
    public static int FAST_GAME_MODE_CREATURE_SPEED_MULTIPLIER = 2;

    public static int SECS_PER_MIN = 60;
    public static int MIN_PER_HR = 60;

    public static int DOUBLE_ZERO = 00;
    
    public static int QUESTS_MODE = 1;

    public static int JOKER_TIME = 5;

    public static int TOTAL_NUMBER_OF_LEVELS = 4;

    public static int ENERGY_INCREASE_PER_CATCH = 5;
    public static int TIME_TO_BE_ALIVE_FOR_ENERGY = 10;

    public static int INITIAL_TIME = Numbers.ZERO;
    public static int INITAL_ENERGY = Numbers.TWO_HUNDRED;
    public static int INITAL_LIVES = Numbers.TWO;

}