﻿using UnityEngine;
using System.Collections;

public class Numbers : MonoBehaviour
{
    public static int ZERO = 0;
    public static int ONE = 1;
    public static int TWO = 2;
    public static int TEN = 10;
    public static int FIFTEEN = 15;
    public static int TWENTY = 20;
    public static int FIFTY_NINE = 59;
    public static int SIXTY = 60;
    public static int TWO_HUNDRED = 200;

}