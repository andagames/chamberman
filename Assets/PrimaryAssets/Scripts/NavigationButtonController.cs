﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NavigationButtonController : MonoBehaviour
{
    public GameObject CrewManagementPanel;
    public GameObject MissionSelectionPanel;
    public GameObject MyShipPanel;
    public GameObject BlackPanel;

    // Use this for initialization
    void Start()
    {
        MissionSelectionPanel.SetActive(false);
        MyShipPanel.SetActive(false);
        BlackPanel.SetActive(false);
        CrewManagementPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void MyShipIconPointerDown()
    {
        Debug.Log("myship icon or button Clicked");
        MyShipPanel.SetActive(true);
        BlackPanel.SetActive(true);
    }

    public void BlakcPanelPointerDown()
    {
        Debug.Log("BlackPanel Clicked");
        MyShipPanel.SetActive(false);
        BlackPanel.SetActive(false);
        CrewManagementPanel.SetActive(false);
        MissionSelectionPanel.SetActive(false);
    }
 /*   public void PlanetJoPointerDown()
    {
        Debug.Log("PlanetJo has been clicked");
        SceneManager.LoadScene("RecordRun");
    }
    */
    public void AcceptMissionPointerDown()
    {
        Debug.Log("AcceptMission has been clicked");
        SceneManager.LoadScene("RecordRun");
    }
    public void DeclineMissionPointerDown()
    {
        Debug.Log("DeclineMission has been clicked");
        MissionSelectionPanel.SetActive(false);
        BlackPanel.SetActive(false);
    }
    public void CrewManagementPointerDown()
    {
        Debug.Log("CrewManagementPointerDown has been clicked");
        CrewManagementPanel.SetActive(true);
        BlackPanel.SetActive(true);
    }
}