﻿using UnityEngine;
using System.Collections;

public class BubbleAnim : MonoBehaviour
{
    private Animator anim;


    public bool IsMoving
    {

        get { return anim.GetBool("IsMoving"); }
        set { anim.SetBool("IsMoving", value); }
    }
    public void Awake()
    {

        anim = GetComponent<Animator>();

    }

    // Update is called once per frame
    public void Update()
    {

    }
}
