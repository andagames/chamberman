﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class MyShipButtonController : MonoBehaviour {


    public GameObject Element1Text;
    public GameObject Element2Text;
    public GameObject Element3Text;
    public GameObject GoldText;

    public GameObject UpgradeButton1;
    public GameObject UpgradePanel1;


    public Sprite UPGRADE_DONE_ICON;
    public Sprite UPGRADE_DONE_PANEL;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Upgrade1ButtonDown()
    {
        Debug.Log("Upgrade1ButtonDown");
        UpgradeButton1.GetComponent<Image>().sprite = UPGRADE_DONE_ICON;
        UpgradePanel1.GetComponent<Image>().sprite = UPGRADE_DONE_PANEL;

    }
}
