﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class CrewManagementButtonController : MonoBehaviour {

    public GameObject PilotNumberText;
    public GameObject BiologNumberText;
    public GameObject SoldierNumberText;
    public GameObject AssasinNumberText;
    public GameObject DoctorNumberText;

    public GameObject CurrentCostText;
    public GameObject CurrentCrewNumberText;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PilotIncreaseButtonDown()
    {
        Debug.Log("PilotIncreaseButtonDown");
        PilotNumberText.GetComponent<TextMeshProUGUI>().text = (int.Parse(PilotNumberText.GetComponent<TextMeshProUGUI>().text) + 1).ToString();
        CurrentCostText.GetComponent<TextMeshProUGUI>().text =(int.Parse(CurrentCostText.GetComponent<TextMeshProUGUI>().text) + 100).ToString();
        CurrentCrewNumberText.GetComponent<TextMeshProUGUI>().text = (int.Parse(CurrentCrewNumberText.GetComponent<TextMeshProUGUI>().text) + 1).ToString();

    }
    public void PilotDecreaseButtonDown()
    {
        Debug.Log("PilotDecreaseButtonDown");
        if ((int.Parse(PilotNumberText.GetComponent<TextMeshProUGUI>().text) > 0)){
            PilotNumberText.GetComponent<TextMeshProUGUI>().text = (int.Parse(PilotNumberText.GetComponent<TextMeshProUGUI>().text) - 1).ToString();
            CurrentCostText.GetComponent<TextMeshProUGUI>().text = (int.Parse(CurrentCostText.GetComponent<TextMeshProUGUI>().text) - 100).ToString();
            CurrentCrewNumberText.GetComponent<TextMeshProUGUI>().text = (int.Parse(CurrentCrewNumberText.GetComponent<TextMeshProUGUI>().text) - 1).ToString();

        }
    }
    public void BiologIncreaseButtonDown()
    {
        Debug.Log("BiologIncreaseButtonDown");
        BiologNumberText.GetComponent<TextMeshProUGUI>().text = (int.Parse(BiologNumberText.GetComponent<TextMeshProUGUI>().text) + 1).ToString();
        CurrentCostText.GetComponent<TextMeshProUGUI>().text = (int.Parse(CurrentCostText.GetComponent<TextMeshProUGUI>().text) + 200).ToString();
        CurrentCrewNumberText.GetComponent<TextMeshProUGUI>().text = (int.Parse(CurrentCrewNumberText.GetComponent<TextMeshProUGUI>().text) + 1).ToString();


    }
    public void BiologDecreaseButtonDown()
    {
        Debug.Log("BiologDecreaseButtonDown");
        if ((int.Parse(BiologNumberText.GetComponent<TextMeshProUGUI>().text) > 0))
        {
            BiologNumberText.GetComponent<TextMeshProUGUI>().text = (int.Parse(BiologNumberText.GetComponent<TextMeshProUGUI>().text) - 1).ToString();
            CurrentCostText.GetComponent<TextMeshProUGUI>().text = (int.Parse(CurrentCostText.GetComponent<TextMeshProUGUI>().text) - 200).ToString();
            CurrentCrewNumberText.GetComponent<TextMeshProUGUI>().text = (int.Parse(CurrentCrewNumberText.GetComponent<TextMeshProUGUI>().text) - 1).ToString();

        }
    }
    public void AssasinIncreaseButtonDown()
    {
        Debug.Log("AssasinIncreaseButtonDown");
        AssasinNumberText.GetComponent<TextMeshProUGUI>().text = (int.Parse(AssasinNumberText.GetComponent<TextMeshProUGUI>().text) + 1).ToString();
        CurrentCostText.GetComponent<TextMeshProUGUI>().text = (int.Parse(CurrentCostText.GetComponent<TextMeshProUGUI>().text) + 150).ToString();
        CurrentCrewNumberText.GetComponent<TextMeshProUGUI>().text = (int.Parse(CurrentCrewNumberText.GetComponent<TextMeshProUGUI>().text) + 1).ToString();

    }
    public void AssasinDecreaseButtonDown()
    {
        Debug.Log("AssasinDecreaseButtonDown");
        if ((int.Parse(AssasinNumberText.GetComponent<TextMeshProUGUI>().text) > 0))
        {
            AssasinNumberText.GetComponent<TextMeshProUGUI>().text = (int.Parse(AssasinNumberText.GetComponent<TextMeshProUGUI>().text) - 1).ToString();
            CurrentCostText.GetComponent<TextMeshProUGUI>().text = (int.Parse(CurrentCostText.GetComponent<TextMeshProUGUI>().text) - 150).ToString();
            CurrentCrewNumberText.GetComponent<TextMeshProUGUI>().text = (int.Parse(CurrentCrewNumberText.GetComponent<TextMeshProUGUI>().text) - 1).ToString();

        }
    }
    public void SoldierIncreaseButtonDown()
    {
        Debug.Log("SoldierIncreaseButtonDown");
        SoldierNumberText.GetComponent<TextMeshProUGUI>().text = (int.Parse(SoldierNumberText.GetComponent<TextMeshProUGUI>().text) + 1).ToString();
        CurrentCostText.GetComponent<TextMeshProUGUI>().text = (int.Parse(CurrentCostText.GetComponent<TextMeshProUGUI>().text) + 300).ToString();
        CurrentCrewNumberText.GetComponent<TextMeshProUGUI>().text = (int.Parse(CurrentCrewNumberText.GetComponent<TextMeshProUGUI>().text) + 1).ToString();

    }
    public void SoldierDecreaseButtonDown()
    {
        Debug.Log("SoldierDecreaseButtonDown");
        if ((int.Parse(SoldierNumberText.GetComponent<TextMeshProUGUI>().text) > 0))
        {
            SoldierNumberText.GetComponent<TextMeshProUGUI>().text = (int.Parse(SoldierNumberText.GetComponent<TextMeshProUGUI>().text) - 1).ToString();
            CurrentCostText.GetComponent<TextMeshProUGUI>().text = (int.Parse(CurrentCostText.GetComponent<TextMeshProUGUI>().text) - 300).ToString();
            CurrentCrewNumberText.GetComponent<TextMeshProUGUI>().text = (int.Parse(CurrentCrewNumberText.GetComponent<TextMeshProUGUI>().text) - 1).ToString();

        }
    }
    public void DoctorIncreaseButtonDown()
    {
        Debug.Log("DoctorIncreaseButtonDown");
        DoctorNumberText.GetComponent<TextMeshProUGUI>().text = (int.Parse(DoctorNumberText.GetComponent<TextMeshProUGUI>().text) + 1).ToString();
        CurrentCostText.GetComponent<TextMeshProUGUI>().text = (int.Parse(CurrentCostText.GetComponent<TextMeshProUGUI>().text) + 250).ToString();
        CurrentCrewNumberText.GetComponent<TextMeshProUGUI>().text = (int.Parse(CurrentCrewNumberText.GetComponent<TextMeshProUGUI>().text) + 1).ToString();

    }
    public void DoctorDecreaseButtonDown()
    {
        Debug.Log("DoctorDecreaseButtonDown");
        if ((int.Parse(DoctorNumberText.GetComponent<TextMeshProUGUI>().text) > 0))
        {
            DoctorNumberText.GetComponent<TextMeshProUGUI>().text = (int.Parse(DoctorNumberText.GetComponent<TextMeshProUGUI>().text) - 1).ToString();
            CurrentCostText.GetComponent<TextMeshProUGUI>().text = (int.Parse(CurrentCostText.GetComponent<TextMeshProUGUI>().text) - 250).ToString();
            CurrentCrewNumberText.GetComponent<TextMeshProUGUI>().text = (int.Parse(CurrentCrewNumberText.GetComponent<TextMeshProUGUI>().text) - 1).ToString();

        }
    }
    public void PurchaseButtonDown()
    {
        Debug.Log("PurchaseButtonDown");

        if (PlayerPrefUtil.getGoldCount() > (int.Parse(CurrentCostText.GetComponent<TextMeshProUGUI>().text))){
            PlayerPrefUtil.setPilotNumber(PlayerPrefUtil.getPilotNumber() + (int.Parse(PilotNumberText.GetComponent<TextMeshProUGUI>().text)));
            PlayerPrefUtil.setBiologNumber(PlayerPrefUtil.getBiologNumber() + (int.Parse(BiologNumberText.GetComponent<TextMeshProUGUI>().text)));
            PlayerPrefUtil.setSoldierNumber(PlayerPrefUtil.getSoldierNumber() + (int.Parse(SoldierNumberText.GetComponent<TextMeshProUGUI>().text)));
            PlayerPrefUtil.setAssasinNumber(PlayerPrefUtil.getAssasinNumber() + (int.Parse(AssasinNumberText.GetComponent<TextMeshProUGUI>().text)));
            PlayerPrefUtil.setDoctorNumber(PlayerPrefUtil.getDoctorNumber() + (int.Parse(DoctorNumberText.GetComponent<TextMeshProUGUI>().text)));

            PlayerPrefUtil.setGoldCount(PlayerPrefUtil.getGoldCount() - (int.Parse(CurrentCostText.GetComponent<TextMeshProUGUI>().text)));

            PilotNumberText.GetComponent<TextMeshProUGUI>().text = "0";
            BiologNumberText.GetComponent<TextMeshProUGUI>().text = "0";
            SoldierNumberText.GetComponent<TextMeshProUGUI>().text = "0";
            AssasinNumberText.GetComponent<TextMeshProUGUI>().text = "0";
            DoctorNumberText.GetComponent<TextMeshProUGUI>().text = "0";
            CurrentCrewNumberText.GetComponent<TextMeshProUGUI>().text = "0";
            CurrentCostText.GetComponent<TextMeshProUGUI>().text = "0";
        }
    }
}
